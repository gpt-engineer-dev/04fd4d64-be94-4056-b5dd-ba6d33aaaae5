let mediaRecorder;
let recordedChunks = [];

document
  .getElementById("startRecording")
  .addEventListener("click", async () => {
    try {
      const stream = await navigator.mediaDevices.getDisplayMedia({
        video: true,
      });
      document.getElementById("screenView").srcObject = stream;
      mediaRecorder = new MediaRecorder(stream);

      mediaRecorder.ondataavailable = (event) => {
        if (event.data.size > 0) {
          recordedChunks.push(event.data);
        }
      };

      mediaRecorder.start();
      document.getElementById("startRecording").disabled = true;
      document.getElementById("stopRecording").disabled = false;
    } catch (error) {
      document.getElementById(
        "errorMessage",
      ).textContent = `Error: ${error.message}`;
    }
  });

document.getElementById("stopRecording").addEventListener("click", () => {
  try {
    mediaRecorder.stop();
    document.getElementById("startRecording").disabled = false;
    document.getElementById("stopRecording").disabled = true;
  } catch (error) {
    document.getElementById(
      "errorMessage",
    ).textContent = `Error: ${error.message}`;
  }
});

document.getElementById("download").addEventListener("click", () => {
  try {
    const blob = new Blob(recordedChunks, {
      type: "video/webm",
    });

    const url = URL.createObjectURL(blob);
    const a = document.getElementById("download");
    a.href = url;
    a.download = "recorded.webm";
  } catch (error) {
    document.getElementById(
      "errorMessage",
    ).textContent = `Error: ${error.message}`;
  }
});
